package com.uid.geometrycalculator.core;

import org.junit.Test;

import static com.uid.geometrycalculator.core.Constants.DELTA;
import static junit.framework.TestCase.assertEquals;

/**
 * Test cases for {@link Ellipse}.
 *
 * @author Jannik Lassahn
 */
public class EllipseTest {
    @Test
    public void Given_EqualHeightAndWidthGreaterZero_When_CalculateEllipse_Then_HasCorrectValues() {
        assertEquals(314.159265, Ellipse.getArea(20, 20), DELTA);
        assertEquals(62.831853, Ellipse.getCircumference(20, 20, 20), DELTA);
    }

    @Test
    public void Given_HeightAndWidthGreaterZero_When_CalculateEllipse_Then_HasCorrectValues() {
        assertEquals(31.416, Ellipse.getArea(20, 2), DELTA);
        assertEquals(40.63974, Ellipse.getCircumference(20, 2, 20), DELTA);
    }
}
