package com.uid.geometrycalculator.core;

import org.junit.*;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Test cases for {@link Creator}.
 *
 * @author Jannik Lassahn
 */
public class CreatorTest {

    @Test
    public void Given_MatchingParameter_When_Validate_Then_ReturnsTrue() {

        Map<String, Double> actual = new HashMap<>();
        actual.put("a", 1.0);
        actual.put("b", 1.0);

        Set<Set<String>> expected = new HashSet<>();
        Set<String> expectedSet1 = new HashSet<>();
        expectedSet1.add("a");
        expectedSet1.add("b");

        Set<String> expectedSet2 = new HashSet<>();
        expectedSet2.add("c");
        expectedSet2.add("d");

        expected.add(expectedSet1);
        expected.add(expectedSet2);

        boolean isValid = Creator.containsSet(expected, actual);

        assertTrue(isValid);
    }

    @Test
    public void Given_InvalidParameter_When_Validate_Then_ReturnsFalse() {
        Map<String, Double> actual = new HashMap<>();
        actual.put("a", 1.0);
        actual.put("b", 1.0);

        Set<Set<String>> expected = new HashSet<>();
        Set<String> expectedSet = new HashSet<>();
        expectedSet.add("a");
        expectedSet.add("c");
        expected.add(expectedSet);

        boolean isValid = Creator.containsSet(expected, actual);

        assertFalse(isValid);
    }

    @Test
    public void Given_ValidInput_When_Parse_Then_ParsedParameter() {
        Optional<Map<String, Double>> parsed = Creator.parse("a=1");

        assertTrue(parsed.isPresent());
        assertEquals(1.0, parsed.get().get("a"), 0.001);
    }

    @Test
    public void Given_MissingValue_When_Parse_Then_NoParsedParameter() {
        Optional<Map<String, Double>> parsed = Creator.parse("a=");
        assertFalse(parsed.isPresent());
    }

    @Test
    public void Given_ValidInput_When_CalculateGeometry_Then_ResultsAvailable() {
        Creator.calculate("Circle", "r=2", map -> {
            assertNotEquals(0, map.get("Area"));
            assertNotEquals(0, map.get("Circumference"));
        }, error -> fail());
    }

    @Test
    public void Given_InvalidType_When_CalculateGeometry_Then_ThrowsException() {
        Creator.calculate("X", "", map -> fail(), org.junit.Assert::assertNotNull);
    }

    @Test
    public void Given_InvalidParameter_When_CalculateGeometry_Then_ThrowsException() {
        Creator.calculate("Circle", "a=2", map -> fail(), org.junit.Assert::assertNotNull);
    }

}
