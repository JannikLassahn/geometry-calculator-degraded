package com.uid.geometrycalculator.console;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * Integration test cases for {@link Program}.
 *
 * @author Jannik Lassahn
 */
public class ProgramIT {

    private static final ByteArrayOutputStream OUT = new ByteArrayOutputStream();
    private static final ByteArrayOutputStream ERR = new ByteArrayOutputStream();

    @BeforeClass
    public static void setUpStreams() {
        System.setOut(new PrintStream(OUT));
        System.setErr(new PrintStream(ERR));
    }

    @AfterClass
    public static void cleanUpStreams() {
        System.setIn(System.in);
        System.setOut(System.out);
        System.setErr(System.err);
    }

    @Test
    public void Given_NoCLIArguments_When_RunProgram_Then_StartGuidedFlow(){

        setUpInput(String.format("1%nr=1%ny%ny%n2%na=2;b=1%nn%nn%n"));
        Program.main(new String[0]);
    }

    @Test
    public void Given_CalcCommand_When_RunProgram_Then_StartCommandFlowAndCalculate(){
        Program.main(new String[]{ "calc", "circle", "r=2" });
    }

    @Test
    public void Given_HistoryCommand_When_RunProgram_Then_StartCommandFlowAndShowHistory(){
        Program.main(new String[]{ "history"});
    }

    @Test
    public void Given_ClearHistoryCommand_When_RunProgram_Then_StartCommandFlowAndClearHistory(){
        Program.main(new String[]{ "history", "--clear"});
    }

    @Test
    public void Given_HelpCommand_When_RunProgram_Then_StartCommandFlowAndShowHelp(){
        Program.main(new String[]{ "?"});
    }

    private void setUpInput(String input) {
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
    }
}
