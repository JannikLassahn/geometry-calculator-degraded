# Geometry Calculator #

[![Coverage Status][coverage-image]][coverage-url]

These projects are part of a school project.

## Projects ###
All projects are managed using Maven.

### GeometryCalculator.Core ###
This project contains algorithms for calculating different shapes.


### GeometryCalculator.Console ###
This project contains a console application for calculating geometries found in GeometryCalculator.Core.

[coverage-image]: https://codecov.io/bb/janniklassahn/geometry-calculator-degraded/branch/master/graph/badge.svg
[coverage-url]: https://codecov.io/bb/janniklassahn/geometry-calculator-degraded