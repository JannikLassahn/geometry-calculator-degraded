package com.uid.geometrycalculator.console;

import com.sun.org.apache.xerces.internal.util.HTTPInputSource;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

/**
 * Test cases for {@link History}.
 *
 * @author Jannik Lassahn
 */
public class HistoryTest {

    @Test
    public void Given_HistoryAndNewValue_When_AddNewEntry_Then_HistoryContainsEntry(){

        double[] history = new double[0];
        history = History.addEntry(history, 1);

        assertEquals(1.0, history[0], 0.001);
    }

    @Test
    public void Given_HistoryWithEntries_When_ClearHistory_Then_NoEntriesInHistory(){

        double[] history = new double[] { 1.0, 2.0 };
        history = History.clearHistory();

        assertEquals(0, history.length);
    }

    @Test
    public void Given_HistoryFile_When_LoadHistory_Then_HistoryContainsEntries(){

        History.saveHistory(new double[]{1.0});
        double[] history = History.loadHistory();

        assertEquals(1.0, history[0], 0.001);
    }

    @Test
    public void Given_HistoryWithEntries_When_PrintHistory_Then_ConsoleContainsText(){

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        History.printHistory(new double[]{ 1.0 });

        assertTrue(out.toString().contains("1.0"));

        System.setOut(System.out);

    }
}
