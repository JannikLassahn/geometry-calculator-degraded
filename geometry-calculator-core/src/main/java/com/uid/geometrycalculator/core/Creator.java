package com.uid.geometrycalculator.core;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Contains methods for calculating geometries.
 *
 * @author Jannik Lassahn
 */
public final class Creator {

    private Creator() {
    }

    //////////////////////////////////////
    // METADATA
    //////////////////////////////////////

    /**
     * Parameter metadata for a radius.
     */
    private final static Map<String, String> RADIUS = new HashMap<String, String>() {
        {
            put("r", "The radius");
        }
    };

    /**
     * Parameter metadata for height and width.
     */
    private final static Map<String, String> HEIGHTWIDTH = new HashMap<String, String>() {
        {
            put("a", "The width");
            put("b", "The height");
        }
    };

    /**
     * Parameter metadata for side A, side B and side C.
     */
    private final static Map<String, String> ABC = new HashMap<String, String>() {
        {
            put("a", "The first side");
            put("b", "The second side");
            put("c", "The third side");
        }
    };

    /**
     * Parameter metadata for an angle, side A and side B.
     */
    private final static Map<String, String> ANGLEAB = new HashMap<String, String>() {
        {
            put("a", "The first side");
            put("b", "The second side");
            put("angle", "The angle between both sides");
        }
    };

    //////////////////////////////////////
    // METHODS
    //////////////////////////////////////

    /**
     * Returns whether a set is contained in another.
     *
     * @param expected
     * @param actual
     * @return True, if the set of keys is in the expected set.
     */
    public static boolean containsSet(Set<Set<String>> expected, Map<String, Double> actual) {

        if(!actual.values().stream().anyMatch(value -> value > 0 && Double.isFinite(value)))
            return false;
        
        return expected.stream().anyMatch(thing -> thing.stream().allMatch(s -> actual.containsKey(s)));
    }

    /**
     * Calculates a geometry.
     *
     * @param type   The type of geometry.
     * @param input  The parameter.
     * @param result The calculated values.
     * @param error  The error (if present).
     */
    public static void calculate(String type, String input, Consumer<Map<String, Double>> result, Consumer<Exception> error) {

        // convert to lower case to prevent unnecessary confusion.
        type = type.toLowerCase();

        // check whether type is present
        if (!GEOMETRIES.containsKey(type)) {
            error.accept(new IllegalArgumentException(String.format("There is no geometry named '%s'.", type)));
            return;
        }

        // parse the input
        Optional<Map<String, Double>> params = parse(input);

        // validate parsed input
        if (!params.isPresent()) {
            error.accept(new IllegalArgumentException("Could not parse parameter."));
            return;
        }

        Set<Set<String>> xs = GEOMETRIES.get(type).stream().map(map -> new HashSet<>(map.keySet())).collect(Collectors.toSet());

        // validate against geometry
        if (containsSet(xs, params.get())) {

            try{
                // calculate
                result.accept(FACTORY.get(type).apply(params.get()));
            } catch (Exception e){
                error.accept(e);
            }

        } else {

            // notify error
            error.accept(new IllegalArgumentException("Invalid parameter"));
        }
    }

    /**
     * Parses input based on regex.
     *
     * @param input The input to parse.
     * @return The parsed key-value parameter.
     */
    public static Optional<Map<String, Double>> parse(String input) {

        Map<String, Double> parsed = new TreeMap<>();
        String[] params = input.split("[=;]");

        try {
            for (int i = 0; i < params.length; i++) {
                parsed.put(params[i++], Double.parseDouble(params[i]));
            }
            return Optional.of(parsed);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    /**
     * Adds a geometry to the geometry and factory maps.
     *
     * @param type    The name of the geometry.
     * @param params  The parameter necessary to create the geometry.
     * @param factory The factory method for calculating the geometry.
     */
    private static void register(String type, Set<Map<String, String>> params, Function<Map<String, Double>, Map<String, Double>> factory) {

        if (!GEOMETRIES.containsKey(type))
            GEOMETRIES.put(type, params);

        if (!FACTORY.containsKey(type))
            FACTORY.put(type, factory);
    }

    /**
     * Registers all geometries in this package.
     */
    private static void registerAll() {

        register("circle",
                withParameter(RADIUS),
                withFactory(
                        map -> Circle.getArea(map.get("r")),
                        map -> Circle.getCircumference(map.get("r"))));

        register("ellipse",
                withParameter(HEIGHTWIDTH),
                withFactory(
                        map -> Ellipse.getArea(map.get("a"), map.get("b")),
                        map -> Ellipse.getCircumference(map.get("a"), map.get("b"), 20)));

        register("rectangle",
                withParameter(HEIGHTWIDTH),
                withFactory(
                        map -> Rectangle.getArea(map.get("a"), map.get("b")),
                        map -> Rectangle.getCircumference(map.get("a"), map.get("b"))));

        register("triangle",
                withParameter(ABC, ANGLEAB),
                withFactory(
                        map -> 
                        {
                            if (map.containsKey("angle")) {
                                return Triangle.getAreaFromSAS(map.get("angle"), map.get("a"), map.get("b"));
                            } else {
                                return Triangle.getArea(map.get("a"), map.get("b"), map.get("c"));
                            }
                        },
                        map -> 
                        {
                            if (map.containsKey("angle")) {
                                return Triangle.getCircumferenceFromSAS(map.get("angle"), map.get("a"), map.get("b"));
                            } else {
                                return Triangle.getCircumference(map.get("a"), map.get("b"), map.get("c"));
                            }
                        }));
    }

    /**
     * Helper method for transforming multiple maps into a set of maps.
     *
     * @param params The parameter.
     * @return The set parameter.
     */
    public static Set<Map<String, String>> withParameter(Map<String, String>... params) {
        Set<Map<String, String>> values = new HashSet<>();
        Arrays.stream(params).forEach(values::add);
        return values;
    }

    /**
     * Helper method for transforming two factory methods into a single one.
     *
     * @param area The area factory method.
     * @param circ The circumference factory method.
     * @return The combined factory function.
     */
    private static Function<Map<String, Double>, Map<String, Double>> withFactory(
            Function<Map<String, Double>, Double> area,
            Function<Map<String, Double>, Double> circ) {
        return map -> new HashMap<String, Double>() {
            {
                put("Area", area.apply(map));
                put("Circumference", circ.apply(map));
            }
        };
    }

    /**
     * Available geometries.
     */
    public final static Map<String, Set<Map<String, String>>> GEOMETRIES = new TreeMap<>();


    /**
     * Factory for calculating
     */
    public final static Map<String, Function<Map<String, Double>, Map<String, Double>>> FACTORY = new HashMap<>();


    static {
        registerAll();
    }
}
