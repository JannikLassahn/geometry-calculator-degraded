package com.uid.geometrycalculator.core;

/**
 * Contains methods for calculating a circle.
 *
 * @author Jannik Lassahn
 */
public final class Circle {

    private Circle() {
    }

    /**
     * Gets the area of a circle.
     *
     * @param radius The radius.
     * @return The area.
     */
    public static double getArea(double radius) {
        return Math.PI * radius * radius;
    }

    /**
     * Gets the circumference of a circle.
     *
     * @param radius The radius.
     * @return The circumference.
     */
    public static double getCircumference(double radius) {
        return Math.PI * 2 * radius;
    }

}
