package com.uid.geometrycalculator.console.flows;

import com.uid.geometrycalculator.console.Common;
import com.uid.geometrycalculator.console.History;
import com.uid.geometrycalculator.core.Creator;

import java.util.Arrays;

import static java.lang.System.out;

/**
 * Contains methods for handling a command-based flow.
 *
 * @author Jannik Lassahn
 */
public final class CommandFlow {

    private CommandFlow() {
    }

    public static void start(String[] args) {
        switch (args[0]) {
            case "c":
            case "calc":
                calculateCommand(removeFirst(args));
                break;
            case "h":
            case "history":
                historyCommand(removeFirst(args));
                break;
            default:
                helpCommand();
        }
    }

    /**
     * Executes the command for calculating a geometry.
     *
     * @param args The command line arguments.
     */
    private static void calculateCommand(String[] args) {

        // execute help command if arguments do not match
        if (args.length < 2) {
            helpCommand();
        } else {
            Creator.calculate(args[0], args[1], map ->
            {
                Common.onResult(map);
                History.addEntry(History.loadHistory(), map.get("Area"));
            }, Common::onError);
        }
    }

    /**
     * Executes the command for history management.
     *
     * @param args The command line arguments.
     */
    private static void historyCommand(String[] args) {

        // clear the history if clear flag is set
        if (args.length > 0 && args[0].equals("--clear")) {
            History.clearHistory();
            out.println("Successfully cleared history.");
        } else {
            History.printHistory(History.loadHistory());
        }
    }

    /**
     * Shows how to use the program.
     */
    private static void helpCommand() {
        String format = "    %1$-30s%2$-30s%n";

        out.printf("Application%n-----------%n");
        out.printf("    The geometry calculator.%n");
        out.printf("Options%n-------%n");
        out.printf(format, "calc <type> <parameter>", "Calculates geometries");
        out.printf(format, "history [--clear]", "Shows the history of calculations. --clear flag removes all entries.");
        out.printf(format, "help", "Shows the help");
    }

    /**
     * Removes the first field of an array.
     *
     * @param arr The array.
     * @return The array without the first field.
     */
    private static String[] removeFirst(String[] arr) {
        if(arr.length <= 1)
            return new String[0];
        return Arrays.copyOfRange(arr, 1, arr.length);
    }

}
