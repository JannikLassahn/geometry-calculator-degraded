package com.uid.geometrycalculator.core;

import org.junit.Test;

import static com.uid.geometrycalculator.core.Constants.DELTA;
import static junit.framework.TestCase.assertEquals;

/**
 * Test cases for {@link Triangle}.
 *
 * @author Jannik Lassahn
 */
public class TriangleTest {

    @Test
    public void Given_ThreeSidesGreaterZero_When_CalculateTriangle_Then_HasCorrectValues() {
        assertEquals(10.82531, Triangle.getArea(5, 5, 5), DELTA);
        assertEquals(15.0, Triangle.getCircumference(5,5,5), DELTA);
    }

    @Test
    public void Given_AngleAndTwoSidesGreaterZero_When_Calculated_Then_HasCorrectValues() {
        assertEquals(10.82531, Triangle.getAreaFromSAS(60,5,5), DELTA);
        assertEquals(15.0, Triangle.getCircumferenceFromSAS(60,5, 5), DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Given_NonConstructableSides_When_CalculateArea_Then_ThrowsException(){
        Triangle.getArea(5,8,3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Given_NonConstructableSides_When_CalculateCircumference_Then_ThrowsException(){
        Triangle.getCircumference(5,8,3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Given_AngleGreater180AndTwoSidesGreaterZero_When_CalculateArea_Then_ThrowsException() {
        Triangle.getAreaFromSAS(180, 1,2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Given_ZeroAngleAndTwoSidesGreaterZero_When_InstantiateTriangle_Then_ThrowsException() {
        Triangle.getAreaFromSAS(0, 1,2);
    }

}
