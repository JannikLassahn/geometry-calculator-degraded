package com.uid.geometrycalculator.console;

import com.uid.geometrycalculator.console.flows.*;

/**
 * Application
 *
 * @author Jannik Lassahn
 */
public final class Program {

    /**
     * Entry point for a console application.
     *
     * @param args Arguments from command line interface.
     */
    public static void main(String[] args) {
        if (args.length > 0)
            CommandFlow.start(args);
        else
            GuidedFlow.start();
    }
}
