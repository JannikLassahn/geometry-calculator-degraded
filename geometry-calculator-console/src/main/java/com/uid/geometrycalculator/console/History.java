package com.uid.geometrycalculator.console;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.System.out;

/**
 * Contains methods for handling a history of calculations.
 *
 * @author Jannik Lassahn
 */
public final class History {

    private History() {
    }

    /**
     * Adds an entry to the history.
     *
     * @param history The existing history.
     * @param value   The entry to add to the history.
     * @return The new history.
     */
    public static double[] addEntry(double[] history, double value) {
        // create a new array by copying the old one
        history = Arrays.copyOf(history, history.length + 1);
        // set the last field
        history[history.length - 1] = value;
        // save
        saveHistory(history);

        return history;
    }

    /**
     * Clears the history.
     *
     * @return An empty array.
     */
    public static double[] clearHistory() {

        try {
            Files.delete(getFilePath());
        } catch (IOException e) {
        }

        return new double[0];
    }

    /**
     * Saves the history to local storage.
     *
     * @param history The history to save.
     */
    public static void saveHistory(double[] history) {
        try {
            List<String> lines = Arrays.stream(history)
                    .mapToObj(Double::toString)
                    .collect(Collectors.toList());
            Files.write(getFilePath(), lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
        }
    }

    /**
     * Loads the history from local storage.
     *
     * @return The history.
     */
    public static double[] loadHistory() {
        try {
            return Files.readAllLines(getFilePath(), Charset.forName("UTF-8"))
                    .stream()
                    .map(Double::parseDouble)
                    .filter(value -> value > 0)
                    .mapToDouble(Double::doubleValue)
                    .toArray();

        } catch (IOException ex) {
            return new double[0];
        }
    }

    /**
     * Prints all entries to the console.
     *
     * @param history The history to print.
     */
    public static void printHistory(double[] history) {

        if(history.length == 0){
            out.println("No history available.");
            return;
        }

        Common.tableHeader(80, "History of calculations (area only)");
        Common.tableLine(80);
        Arrays.stream(history)
                .mapToObj(Double::toString)
                .forEach(content -> Common.tableContent(80, content));
        Common.tableLine(80);
    }

    /**
     * Gets the path to the file containing all history entries.
     *
     * @return The path.
     */
    private static Path getFilePath() {
        return Paths.get(System.getProperty("java.io.tmpdir"), "gcalc.history");
    }

}
