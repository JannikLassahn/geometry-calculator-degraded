package com.uid.geometrycalculator.core;

import java.util.function.Function;

/**
 * Contains methods for calculating an ellipse.
 *
 * @author Jannik Lassahn
 */
public final class Ellipse {

    private Ellipse() {
    }

    /**
     * Gets the area of an ellipse.
     *
     * @param height The height.
     * @param width The width.
     * @return The area.
     */
    public static double getArea(double width, double height) {
        return Math.PI * height * width * 0.25;
    }

    /**
     * Gets the circumference of an ellipse.
     *
     * @param height The height.
     * @param width The width.
     * @param iterations    The number of iterations. More iterations give more accurate results.
     * @return The circumference.
     */
    public static double getCircumference(double width, double height, int iterations) {

        double semiMajorAxis = width / 2;
        double semiMinorAxis = height / 2;

        // Bessel(1825).
        //
        //      a: Semi-Major Axis
        //      b: Semi-Minor Axis
        //
        //      h = (a - b)² / (a + b)²
        //
        //                           INFINITY   (2i-3)!!
        //      U = PI * (a + b) * [ SUM      ( -------- )² * h^i + 1 + (1/2)² * h ]
        //                           i = 2       (2i)!!
        //
        // This series converges quickly to get an accurate value (compared to other approximation-based algorithms).
        // We will never get the "real" value, but the error delta with more than 10 iterations is incredibly small.
        double result = 1;
        double sum;

        double h = Math.pow(semiMajorAxis - semiMinorAxis, 2) / Math.pow(semiMajorAxis + semiMinorAxis, 2);

        for (int i = 1; i < iterations; i++) {
            sum = 1;

            for (int j = i; j > 0; j--) {
                if (j > 1) {
                    sum *= 2 * (j - 1) - 1;
                }
                sum /= 2 * j;
            }
            result += Math.pow(h, i) * sum * sum;
        }

        return result * Math.PI * (semiMajorAxis + semiMinorAxis);
    }
}
