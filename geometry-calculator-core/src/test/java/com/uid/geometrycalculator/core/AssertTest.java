package com.uid.geometrycalculator.core;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test cases for {@link Assert}.
 *
 * @author Jannik Lassahn
 */
public class AssertTest {

    @Test
    public void Given_NotNull_When_AssertingNotNull_Then_ThrowsNoException() {
        Assert.notNull(this);
        // no junit assertion required
    }

    @Test
    public void Given_NotNullAndMessage_When_AssertingNotNull_Then_ThrowsNoException() {
        Assert.notNull(this, "test");
        // no junit assertation required
    }

    @Test(expected = IllegalArgumentException.class)
    public void Given_Null_When_AssertingNotNull_Then_ThrowsException() {
        Assert.notNull(null);
        // no junit assertion required
    }

    @Test
    public void Given_NullAndMessage_When_AssertingNotNull_Then_ThrowsExceptionWithMessage() {
        try {
            Assert.notNull(null, "test");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
    }

    @Test
    public void Given_GreaterZero_When_AssertingNotNegativeOrZero_Then_ThrowsNoException() {
        Assert.notNegativeOrZero(1);
    }

    @Test
    public void Given_GreaterZeros_When_AssertingNotNegativeOrZero_Then_ThrowsNoException(){
        Assert.notNegativeOrZero(1,2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Given_Zero_When_AssertingNotNegativeOrZero_Then_ThrowsException() {
        Assert.notNegativeOrZero(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Given_Negative_When_AssertingNotNegativeOrZero_Then_ThrowsException() {
        Assert.notNegativeOrZero(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Given_Negatives_When_AssertingNotNegativeOrZero_Then_ThrowsException(){
        Assert.notNegativeOrZero(1,2,0);
    }
}
