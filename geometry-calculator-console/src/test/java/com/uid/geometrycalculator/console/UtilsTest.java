package com.uid.geometrycalculator.console;

import java.io.ByteArrayInputStream;
import java.util.*;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test cases for {@link Utils}.
 *
 * @author Jannik Lassahn
 */
public class UtilsTest {

    @Test
    public void Given_AskingForYesNo_When_EnteringYes_Then_ReturnTrue() {
        ByteArrayInputStream in = new ByteArrayInputStream("Yes".getBytes());

        Boolean yes = Utils.getYesOrNo(new Scanner(in));

        assertTrue(yes);
    }

    @Test
    public void Given_AskingForYesNo_When_EnteringY_Then_ReturnTrue() {
        ByteArrayInputStream in = new ByteArrayInputStream("y".getBytes());

        Boolean yes = Utils.getYesOrNo(new Scanner(in));

        assertTrue(yes);
    }

    @Test
    public void Given_AskingForYesNo_When_EnteringNo_Then_ReturnFalse() {
        ByteArrayInputStream in = new ByteArrayInputStream("No".getBytes());

        Boolean no = Utils.getYesOrNo(new Scanner(in));

        assertFalse(no);
    }

    @Test
    public void Given_AskingForYesNo_When_EnteringN_Then_ReturnFalse() {
        ByteArrayInputStream in = new ByteArrayInputStream("n".getBytes());

        Boolean no = Utils.getYesOrNo(new Scanner(in));

        assertFalse(no);
    }

    @Test
    public void Given_AskingForYesNo_When_EnteringWrongValue_Then_AskAgain() {
        ByteArrayInputStream in = new ByteArrayInputStream(String.format("x%ny").getBytes());

        Boolean yes = Utils.getYesOrNo(new Scanner(in));

        assertTrue(yes);
    }

    @Test
    public void Given_AskingForValueInRange_When_EnteringValueInRange_Then_ReturnValue() {
        ByteArrayInputStream in = new ByteArrayInputStream("1".getBytes());

        int value = Utils.getFromRange(new Scanner(in), 0, 1);

        assertEquals(1, value);
    }

    @Test
    public void Given_AskingForValueInRange_When_EnteringNotInteger_Then_AskAgain() {
        ByteArrayInputStream in = new ByteArrayInputStream(String.format("x%n1").getBytes());

        int value = Utils.getFromRange(new Scanner(in), 0, 1);

        assertEquals(1, value);
    }

    @Test
    public void Given_AskingForValueInRange_When_EnteringValueOutOfRange_Then_AskAgain() {
        ByteArrayInputStream in = new ByteArrayInputStream(String.format("2%n1").getBytes());

        int value = Utils.getFromRange(new Scanner(in), 0, 1);

        assertEquals(1, value);
    }

    @Test
    public void Given_DashAndAmount_When_Repeat_Then_LineOfDashes() {
        assertEquals("---", Utils.repeat("-", 3));
    }

    @Test
    public void Given_DashAndNegativeAmount_When_Repeat_Then_NoLine() {
        assertEquals("", Utils.repeat("-", -1));
    }

    @Test
    public void Given_Map_When_ValidIndex_Then_ReturnKeyForIndex() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);

        int value = Utils.getKeyFromIndex(set, 1);

        assertEquals(2, value);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Given_Map_When_InvalidIndex_Then_ThrowException() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        int value = Utils.getKeyFromIndex(set, 2);
    }

}
