package com.uid.geometrycalculator.core;

import java.util.Arrays;

/**
 * Assertion utility class that assists in validating arguments.
 * Useful for identifying programmer errors early and clearly at runtime.
 *
 * @author Jannik Lassahn
 */
public final class Assert {

    private Assert() {
    }

    /**
     * Assert that an object is not null.
     *
     * @param object The object to check.
     * @throws IllegalArgumentException if the object is null.
     */
    public static void notNull(Object object) {
        if (object == null)
            throw new IllegalArgumentException("Assertion failed - the argument must not be null");
    }

    /**
     * Assert that an object is not null.
     *
     * @param object  The object to check.
     * @param message The exception message to use if an assertation fails.
     * @throws IllegalArgumentException if the object is null.
     */
    public static void notNull(Object object, String message) {
        if (object == null)
            throw new IllegalArgumentException(message);
    }

    /**
     * Assert that a floating point number is not smaller or equal to zero.
     *
     * @param number The number to check.
     * @throws IllegalArgumentException if the number is greator or equal zero.
     */
    public static void notNegativeOrZero(double number) {
        if (number <= 0)
            throw new IllegalArgumentException("Assertion failed - the argument must not be zero or negative");
    }

    /**
     * Assert that floating point numbers are not smaller or equal to zero.
     *
     * @param values
     */
    public static void notNegativeOrZero(double... values) {
        Arrays.stream(values).forEach(Assert::notNegativeOrZero);
    }
}