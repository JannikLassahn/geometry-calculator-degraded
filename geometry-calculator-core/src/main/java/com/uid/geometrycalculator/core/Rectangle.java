package com.uid.geometrycalculator.core;

/**
 * Contains methods for calculating a rectangle.
 *
 * @author Jannik Lassahn
 */
public final class Rectangle {

    private Rectangle() {
    }

    /**
     * Gets the area of a rectangle.
     *
     * @param a The width.
     * @param b The height.
     * @return The area.
     */
    public static double getArea(double a, double b) {
        return a * b;
    }

    /**
     * Gets the circumference if a rectangle.
     *
     * @param a The width.
     * @param b The height.
     * @return The circumference.
     */
    public static double getCircumference(double a, double b) {
        return 2 * (a + b);
    }

}
