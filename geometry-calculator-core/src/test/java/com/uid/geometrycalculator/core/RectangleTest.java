package com.uid.geometrycalculator.core;

import org.junit.Test;

import static com.uid.geometrycalculator.core.Constants.DELTA;
import static junit.framework.TestCase.assertEquals;

/**
 * Test cases for {@link Rectangle}.
 *
 * @author Jannik Lassahn
 */
public class RectangleTest {
    @Test
    public void Given_HeightAndWidthGreaterZero_When_CalculateRectangle_Then_HasCorrectValues(){
        assertEquals(20, Rectangle.getArea(10, 2), DELTA);
        assertEquals(24, Rectangle.getCircumference(10, 2), DELTA);
    }
}
