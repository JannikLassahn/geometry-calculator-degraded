package com.uid.geometrycalculator.core;

import org.junit.Test;

import static com.uid.geometrycalculator.core.Constants.*;
import static junit.framework.TestCase.assertEquals;

/**
 * Test cases for {@link Circle}.
 *
 * @author Jannik Lassahn
 */
public class CircleTest {
    @Test
    public void Given_RadiusGreaterZero_When_CalculateCircle_Then_HasCorrectValues() {
        assertEquals(314.159265, Circle.getArea(10), DELTA);
        assertEquals(62.831853, Circle.getCircumference(10), DELTA);
    }
}
