package com.uid.geometrycalculator.console;

import java.util.Map;
import java.util.Scanner;

import static java.lang.System.out;

import java.util.Collections;
import java.util.Set;

/**
 * Utilities for working with the command line.
 *
 * @author Jannik Lassahn
 */
public final class Utils {

    private Utils() {
    }

    /**
     * Gets a Boolean based on the users choice.
     *
     * @param scanner The scanner used for input.
     * @return 'yes' or 'y' returns true. 'no' or 'n' returns false.
     */
    public static boolean getYesOrNo(Scanner scanner) {
        while (true) {
            String s = scanner.next().toLowerCase();
            switch (s) {
                case "y":
                case "yes":
                    return true;
                case "n":
                case "no":
                    return false;
                default:
                    out.print("Invalid character. Please try again (y/n): ");
                    break;
            }
        }
    }

    /**
     * Gets a number in a given range.
     *
     * @param scanner The scanner used for input.
     * @param from    The first number in the range. Smaller than 'to'.
     * @param to      The last number in the range. Larger than 'from'.
     * @return The number in the range (including start and end).
     */
    public static int getFromRange(Scanner scanner, int from, int to) {
        while (true) {
            try {
                int i = scanner.nextInt();
                if (i >= from && i <= to) {
                    return i;
                } else {
                    out.print(String.format("Number out of range (from %1d to %2d). Please try again: ", from, to));
                    scanner.nextLine();
                }

            } catch (Exception e) {
                out.print("Invalid input. Please try again: ");
                scanner.nextLine();
            }
        }
    }

    /**
     * Prints a repeated string to the console.
     *
     * @param s      The string to repeat.
     * @param length How often to repeat the string.
     * @return The repeated string.
     */
    public static String repeat(String s, int length){
        if (length <= 0)
            return "";

        return String.join("", Collections.nCopies(length, s));
    }

    /**
     * Gets the key from a map based on its index.
     *
     * @param <T>   The key type.
     * @param index The index of the key. Starts at zero.
     * @return The key at the index.
     */
    public static <T> T getKeyFromIndex(Set<T> set, int index) {
        int i = 0;
        for (T s : set) {
            if (i == index) {
                return s;
            }
            i++;
        }
        throw new IllegalArgumentException("Cannot find value for index " + index);
    }
}
