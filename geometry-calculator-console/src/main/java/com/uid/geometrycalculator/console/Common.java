package com.uid.geometrycalculator.console;

import java.util.Map;

import static java.lang.System.out;

/**
 * Contains methods that are common with other parts of the application.
 *
 * @author Jannik Lassahn
 */
public final class Common {

    private Common() {
    }

    /**
     * Prints the name of the application (in a very stylish way) to the console.
     */
    public static void printHeader() {
        out.println("   ___                          _              ");
        out.println("  / _ \\___  ___  _ __ ___   ___| |_ _ __ _   _ ");
        out.println(" / /_\\/ _ \\/ _ \\| '_ ` _ \\ / _ \\ __| '__| | | |");
        out.println("/ /_\\\\  __/ (_) | | | | | |  __/ |_| |  | |_| |");
        out.println("\\____/\\___|\\___/|_| |_| |_|\\___|\\__|_|   \\__, |");
        out.println("                                         |___/ ");
        out.println("   ___      _            _       _             ");
        out.println("  / __\\__ _| | ___ _   _| | __ _| |_ ___  _ __ ");
        out.println(" / /  / _` | |/ __| | | | |/ _` | __/ _ \\| '__|");
        out.println("/ /__| (_| | | (__| |_| | | (_| | || (_) | |   ");
        out.println("\\____/\\__,_|_|\\___|\\__,_|_|\\__,_|\\__\\___/|_|   ");
        out.println();
    }

    /**
     * Prints an exception to the console.
     *
     * @param e The exception.
     */
    public static void onError(Exception e) {
        out.printf("Could not calculate geometry. Reason: %s%n", e.getMessage());
    }

    public static void onResult(Map<String, Double> result) {
        tableHeader(80, "Results");
        tableLineSplitted(20, 57);
        tableContent(20, 57, "Area", Double.toString(result.get("Area")));
        tableContent(20, 57, "Circumference", Double.toString(result.get("Circumference")));
        tableLineSplitted(20, 57);
    }

    /**
     * Prints the header of a table.
     *
     * @param width The width of the header.
     * @param title The title.
     */
    public static void tableHeader(int width, String title) {
        out.printf("%n+%s+%n", Utils.repeat("=", width - 2));
        out.printf("| %0$-" + (width - 3) + "s|%n", title);
    }

    /**
     * Prints a line in a table.
     *
     * @param width The width of the line.
     */
    public static void tableLine(int width) {
        out.printf("+%s+%n", Utils.repeat("=", width - 2));
    }

    /**
     * Prints a line with respect to two columns.
     *
     * @param firstColumnWidth  The width of the first column.
     * @param secondColumnWidth The width of the second column.
     */
    public static void tableLineSplitted(int firstColumnWidth, int secondColumnWidth) {
        out.printf("+%s+%s+%n", Utils.repeat("=", firstColumnWidth), Utils.repeat("=", secondColumnWidth));
    }

    /**
     * Prints a table row with one column.
     *
     * @param columnWidth The width of the column.
     * @param content     The content of the row.
     */
    public static void tableContent(int columnWidth, String content) {
        out.printf("| %-" + (columnWidth - 3) + "s|%n", content);
    }

    /**
     * Prints a table row with two column.
     *
     * @param firstColumnWidth  The width of the first column.
     * @param secondColumnWidth The width of the second column.
     * @param firstColumn       The content of the first column.
     * @param secondColumn      The content of the second column.
     */
    public static void tableContent(int firstColumnWidth, int secondColumnWidth, String firstColumn, String secondColumn) {
        out.printf("| %-" + (firstColumnWidth - 1) + "s| %-" + (secondColumnWidth - 1) + "s|%n", firstColumn, secondColumn);
    }

    /**
     * Prints a table row with two columns. The first column expects an index.
     *
     * @param firstColumnWidth  The width of the first column.
     * @param secondColumnWidth The width of the second column.
     * @param indexColumn       The content of the first column.
     * @param secondColumn      The content of the second column.
     */
    public static void tableIndex(int firstColumnWidth, int secondColumnWidth, int indexColumn, String secondColumn) {
        out.printf("| %-" + (firstColumnWidth - 1) + "d| %-" + (secondColumnWidth - 1) + "s|%n", indexColumn, secondColumn);
    }
}



