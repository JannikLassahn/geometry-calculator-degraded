package com.uid.geometrycalculator.core;

/**
 * Contains methods for calculating a triangle.
 *
 * @author Jannik Lassahn
 */
public final class Triangle {

    private Triangle() {
    }

    private static double getThirdSideFromSAS(double angle, double a, double b) {
        if (angle >= 180f || angle <= 0f) {
            throw new IllegalArgumentException("Angle must be >0 and <180 degrees.");
        }

        // we are using the cosine rule to calculate the last side (given sides a and b and angle).
        //      c� = a� + b� - 2 * a * b * COS(angle)
        return Math.sqrt(a * a + b * b - 2.0 * a * b * Math.cos(Math.toRadians(angle)));
    }

    private static boolean isConstructable(double a, double b, double c) {
        return a + b > c && a + c > b && b + c > a;
    }

    /**
     * Gets the area of a triangle based on two sides and the enclosing angle.
     *
     * @param angle The angle between a and b.
     * @param a     The first side.
     * @param b     The second side.
     * @return The area.
     */
    public static double getAreaFromSAS(double angle, double a, double b) {
        return getArea(a, b, getThirdSideFromSAS(angle, a, b));
    }

    /**
     * Gets the circumference of a triangle based on two sides and the enclosing angle.
     *
     * @param angle The angle between a and b.
     * @param a     The first side.
     * @param b     The second side.
     * @return The circumference.
     */
    public static double getCircumferenceFromSAS(double angle, double a, double b) {
        return getCircumference(a, b, getThirdSideFromSAS(angle, a, b));
    }

    /**
     * Gets the area from a triangle based on three sides.
     *
     * @param a The first side.
     * @param b The second side.
     * @param c The third side.
     * @return The area.
     */
    public static double getArea(double a, double b, double c) {

        if (!isConstructable(a, b, c))
            throw new IllegalArgumentException("Triangle is not constructable");

        // we are using Heron's formula for calculating the area.
        //      s = (a + b + c) / 2
        //      A = SQRT(s * (s-a) * (s-b) * (s-c)

        double s = getCircumference(a, b, c) / 2d;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }

    /**
     * Gets the circumference from a triangle based on three sides.
     *
     * @param a The first side.
     * @param b The second side.
     * @param c The third side.
     * @return The circumference.
     */
    public static double getCircumference(double a, double b, double c) {

        if (!isConstructable(a, b, c))
            throw new IllegalArgumentException("Triangle is not constructable");

        return a + b + c;
    }

}
