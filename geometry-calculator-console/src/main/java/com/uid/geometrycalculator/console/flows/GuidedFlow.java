package com.uid.geometrycalculator.console.flows;

import com.uid.geometrycalculator.console.Common;
import com.uid.geometrycalculator.console.History;
import com.uid.geometrycalculator.console.Utils;
import com.uid.geometrycalculator.core.Creator;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;
import static com.uid.geometrycalculator.console.Common.*;

/**
 * Contains methods for a guided flow.
 *
 * @author Jannik Lassahn
 */
public final class GuidedFlow {

    /**
     * Starts the guided flow.
     */
    public static void start() {
        printApplicationHeader();
        runGuide();
    }


    /**
     * Prints the header of the application.
     */
    private static void printApplicationHeader() {

        Common.printHeader();

        out.println();
        out.println("Welcome to the GEOMETRY CALCULATOR (GCALC).");
        out.println("Now you can calculate different types of geometry, either directly through command line arguments (use help command) or guided within the app.");
        out.println("This program uses an independent measurement system. All calculated values are displayed in 'unit(s)'.");
    }

    /**
     * The actual loop that keeps the program/flow running.
     */
    private static void runGuide() {

        String type;
        Scanner scanner = new Scanner(System.in);

        do {
            // 1.   Print all available geometries.
            printGeometries();
            // 2.   Ask user to choose one.
            type = getChosenGeometry(scanner);
            // 3.   Print all necessary parameters.
            printParameters(type);
            // 4.   Ask user to enter parameters.
            // 5.   Calculate geometry.
            calculateGeometry(type, scanner);
            // 6.   Ask for history view.
            showHistory(scanner);
            // 7.   Ask user for further calculations.
            out.print("Would you like to calculate something else? (y/n): ");

        } while (Utils.getYesOrNo(scanner));
    }

    /**
     * Prints a custom table header.
     *
     * @param content The content.
     */
    private static void printTableHeader(String content) {
        tableHeader(80, content);
        tableLineSplitted(7, 70);
    }

    /**
     * Prints all available geometries.
     */
    private static void printGeometries() {
        printTableHeader("Available geometries");
        AtomicInteger counter = new AtomicInteger(1);
        Creator.GEOMETRIES.forEach((name, x) -> tableIndex(7, 70, counter.getAndIncrement(), name));
        tableLineSplitted(7, 70);
    }

    /**
     * Gets the geometry to calculate based on user input.
     *
     * @param scanner The scanner instance.
     * @return The type of geometry to calculate.
     */
    private static String getChosenGeometry(Scanner scanner) {
        out.print("Please enter the geometry you would like to calculate: ");
        return Utils.getKeyFromIndex(Creator.GEOMETRIES.keySet(), Utils.getFromRange(scanner, 1, Creator.GEOMETRIES.size()) - 1);
    }

    /**
     * Prints parameter for a given type to the console.
     *
     * @param type The geometry.
     */
    private static void printParameters(String type) {
        printTableHeader("Parameter for " + type);

        Creator.GEOMETRIES.get(type).forEach(params -> {
            params.forEach((name, help) -> tableContent(7, 70, name, help));
            tableLineSplitted(7, 70);
        });
        out.println("... in the format: NAME=VALUE; (e.g. a=1;b=2.9)");
    }

    /**
     * Calculates and prints the result to the console.
     *
     * @param type    The geometry to calculate.
     * @param scanner The scanner.
     */
    private static void calculateGeometry(String type, Scanner scanner) {
        out.print("Your parameter: ");
        Creator.calculate(
                type,
                scanner.next(),
                result -> {
                    Common.onResult(result);
                    History.addEntry(History.loadHistory(), result.get("Area"));
                },
                Common::onError);
    }

    /**
     * Shows the history.
     *
     * @param scanner The scanner.
     */
    private static void showHistory(Scanner scanner) {

        out.print("Would you like to see a history of your calculations? (y/n): ");
        if (Utils.getYesOrNo(scanner)) {
            History.printHistory(History.loadHistory());
        }
    }
}

