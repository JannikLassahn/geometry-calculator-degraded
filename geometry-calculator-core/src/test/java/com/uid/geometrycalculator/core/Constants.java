package com.uid.geometrycalculator.core;

/**
 * Constants used in tests.
 *
 * @author Jannik Lassahn
 */
public final class Constants {

    public static final double DELTA = 0.001;

}
